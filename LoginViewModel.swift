//
//  LoginViewModel.swift
//  ChillApp-app
//
//  Created by Xavier Algarra on 6/7/16.
//  Copyright © 2016 LVDOC S.L. All rights reserved.
//

import Foundation
import Bolts

enum ResultType {
    case SuccesNew
    case SuccesOld
    case Error
}

typealias loginResponse = (result: ResultType, error: Int?) -> Void

struct loginViewmModel{
    var user: String = ""
    var pass: String = ""
    func login(user _user: String, pass _pass: String, oncallback: loginResponse ) {
        let user : UserEntityBox = UserEntityBox(ParseUserEntity())
        user.username = _user
        user.email = _user
        user.password = _pass
        
        let repository : EntityRepository<UserEntityBox> = EntityRepository(ParseUserEntityRepository())
        repository.login(user.email!, password: user.password!).continueWithBlock { (task: BFTask!) -> BFTask in
            if task.cancelled {
                
            } else if task.error != nil {
                
                oncallback(result: ResultType.Error, error: task.error!.code)
                
            } else {
                
                oncallback(result: ResultType.SuccesOld, error: nil)
                
            }
            return task
        }
    }
}



