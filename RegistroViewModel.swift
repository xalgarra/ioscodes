//
//  RegistroViewModel.swift
//  ChillApp-app
//
//  Created by Xavier Algarra on 15/6/16.
//  Copyright © 2016 LVDOC S.L. All rights reserved.
//

import Foundation
import Parse

enum registerResultType {
    case Succes
    case Error
}

typealias RegisterResponse = (result: registerResultType) -> Void

enum item: Int {
    case user
    case email
    case password
    case birthdate
    case complexion
    case sexualPreference
    case urbanTribe
    case weight
    case height
    
}





struct RegistroViewModel: LabelWithTextAndPickerPresentable {

    var label: String
    var ph: String
    var txt: String
    var elements: [String]
    var items: Int
    
    
    let repository : EntityRepository<UserEntityBox> = EntityRepository(ParseUserEntityRepository())
    var user : UserEntityBox?
    
    init () {
        if PFUser.currentUser() != nil {
            user = repository.current
        }
        
        label = ""
        ph = ""
        txt = ""
        elements = []
        items = 0
    }
    
    init(item _item: Int){
        if PFUser.currentUser() != nil {
            user = repository.current
        }
        
        items = _item
        
        switch _item {
        case item.user.rawValue:
            elements = [""]
            label = "user_account_username_prefix".localized
            ph = "user_account_username_prefix".localized
            guard user != nil else {
                txt = ""
                return
            }
            guard let username = user!.nick else {
                txt = ""
                return
            }
            txt = username
            
        case item.email.rawValue:
            elements = [""]
            label = "user_account_email_prefix".localized
            ph = "user_account_email_prefix".localized
            guard user != nil else {
                txt = ""
                return
            }
            guard let email = user!.email else {
                txt = ""
                return
            }
            txt = email
            
        case item.password.rawValue:
            elements = [""]
            label = "login_password_english_hint".localized
            ph = "login_password_english_hint".localized
            guard user != nil else {
                txt = ""
                return
            }
            guard let password = user!.password else {
                txt = ""
                return
            }
            txt = password
            
        case item.birthdate.rawValue:
            elements = [""]
            label = "user_account_birthday".localized
            ph = "user_account_birthday".localized
            guard user != nil else {
                txt = ""
                return
            }
            guard let birthday = user!.birthday else {
                txt = ""
                return
            }
            let date = birthday
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            let year =  components.year
            let month = components.month
            let day = components.day
            txt = "\(day)/\(month)/\(year)"
            
        case item.complexion.rawValue:
            label = "filters_complexion_title".localized
            ph = ""
            elements = ["complexion_preferences[0]".localized,"complexion_preferences[1]".localized,"complexion_preferences[2]".localized,"complexion_preferences[3]".localized,"complexion_preferences[4]".localized,"complexion_preferences[5]".localized]
            guard user != nil else {
                txt = ""
                return
            }
            guard let complexion = user!.complexion else {
                txt = ""
                return
            }
            txt = complexion
            
        case item.sexualPreference.rawValue:
            label = "filters_sexual_preferences_title".localized
            ph = ""
            elements = ["sexual_preferences[0]".localized, "sexual_preferences[1]".localized, "sexual_preferences[2]".localized, "sexual_preferences[3]".localized]
            guard user != nil else {
                txt = ""
                return
            }
            guard let role = user!.role else {
                txt = ""
                return
            }
            txt = role
        case item.urbanTribe.rawValue:
            label = "user_account_urban_tribe_title".localized
            ph = ""
            elements = ["tribu_urbana_preferences[0]".localized,"tribu_urbana_preferences[1]".localized,"tribu_urbana_preferences[2]".localized,"tribu_urbana_preferences[3]".localized,"tribu_urbana_preferences[4]".localized,"tribu_urbana_preferences[5]".localized,"tribu_urbana_preferences[6]".localized,"tribu_urbana_preferences[7]".localized,"tribu_urbana_preferences[8]".localized,"tribu_urbana_preferences[9]".localized,"tribu_urbana_preferences[10]".localized,"tribu_urbana_preferences[11]".localized,"tribu_urbana_preferences[12]".localized]
            guard user != nil else {
                txt = ""
                return
            }
            guard let urbantribe = user!.tribe else {
                txt = ""
                return
            }
            txt = urbantribe
        case item.weight.rawValue:
            label = "user_account_weight_prefix".localized
            ph = ""
            elements = [""]
            guard user != nil else {
                txt = ""
                return
            }
            guard let weight = user!.weight else {
                txt = ""
                return
            }
            txt = "\(weight)" + "filters_weight_suffix".localized
        case item.height.rawValue:
            label = "user_account_height_prefix".localized
            ph = ""
            elements = [""]
            guard user != nil else {
                txt = ""
                return
            }
            guard let height = user!.height else {
                txt = ""
                return
            }
            txt = "\(height)" + "filters_height_suffix".localized
        default:
            label = ""
            ph = ""
            txt = ""
            elements = [""]
        }
    }
    
    func Registro(data: Dictionary<Int,AnyObject>, image: NSData, oncallback: RegisterResponse) {
        //instanciamos entidad usuario y creamos el usuario si no existe
        
        data.forEach { (par: (Int, AnyObject)) in
            switch par.0 {
            case item.user.rawValue:
                user!.nick = par.1 as? String
                user!.nicksearch = par.1 as? String
            case item.email.rawValue:
                user!.email = par.1 as? String
                user!.username = par.1 as? String
            case item.password.rawValue:
                user!.password = par.1 as? String
            case item.birthdate.rawValue:
                user!.birthday = par.1 as? NSDate
                let calendar : NSCalendar = NSCalendar.currentCalendar()
                let ageComponents = calendar.components(.Year,
                    fromDate: (par.1 as? NSDate)!,
                    toDate: NSDate(),
                    options: [])
                user!.age = ageComponents.year
            case item.complexion.rawValue:
                user!.complexion = par.1 as? String
            case item.height.rawValue:
                user!.height = Double((par.1 as? String)!.deletesuffix())
            case item.weight.rawValue:
                user!.weight = Double((par.1 as? String)!.deletesuffix())
            case item.urbanTribe.rawValue:
                user!.tribe = par.1 as? String
            case item.sexualPreference.rawValue:
                user!.role = par.1 as? String
            default:
                print(par.0)
                print(par.1)
            }
        }
        user!.picture = image
        
        
        repository.save(user!)
        
        oncallback(result: .Succes)
        
    }
    
    func getImage() -> NSData?{
        if PFUser.currentUser() != nil {
            guard let picture = repository.current.picture else {
                return nil
            }
            return picture
        }
        return nil
    }
    
}

// MARK: TextPresentable Conformance
extension RegistroViewModel {
    
    var placeholder: String { return ph }
    
    var text: String { return txt}
}

// MARK: SwitchPresentable Conformance
extension RegistroViewModel {
    
    var labeltext: String { return label }
    
}