//
//  Copyright © 2016 LVDOC S.L. All rights reserved.
//

import Foundation

protocol ImagePresentabe {
    var asset: Asset? { get }
    var image: NSData { get }
}

protocol TextFieldPresentable {
    var placeholder: String { get }
    var text: String { get }
}

protocol LabelPresentabe {
    var labeltext: String { get }
}

protocol TwoLabelPresentabe {
    var firstLabel: String { get }
    var secondLabel: String { get }
}

protocol PickerPresentabe {
    var elements: [String] { get }
}