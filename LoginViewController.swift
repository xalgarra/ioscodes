import UIKit
import PasswordTextField
import SwiftMessageBar
import SlideMenuControllerSwift

class LoginViewController: UIViewController {
    
    @IBOutlet var userTextData: UITextField!
    
    @IBOutlet var passTextData: PasswordTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        CAGradientLayer.applyCustomGradientLayer(self.view) //aplicamos el degradado a la vista
        self.view.addBackground()
        //self.view.backgroundColor = UIColor.getMainBackgroundImage() //aplicamos el fondo de pantalla
        UINavigationBar.applyTransparentColorNavBar(self.navigationController!.navigationBar)
        userTextData.placeholder = "login_email_hint".localized
        passTextData.placeholder = "login_password_hint".localized
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Login(sender: AnyObject) {
        
        let vm = loginViewmModel()
        vm.login(user: userTextData.text!, pass: passTextData.text!, oncallback: {(loginAnswer) -> Void in
            
            if loginAnswer.result == ResultType.Error{
                
                switch loginAnswer.error! {
                case ParseError.EmailMissing.rawValue:
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        SwiftMessageBar.showMessageWithTitle("Error", message: "login_wrong_password".localized , type: .Error)
                        
                    })
                    
                    
                case ParseError.EmailNotFound.rawValue:
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        SwiftMessageBar.showMessageWithTitle("Error", message: "login_invalid_email".localized , type: .Error)
                    })
                    
                    
                case ParseError.InvalidEmailAddress.rawValue:
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        SwiftMessageBar.showMessageWithTitle("Error", message: "login_fill_email".localized , type: .Error)
                    })
                    
                    
                case ParseError.UsernameTaken.rawValue:
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        SwiftMessageBar.showMessageWithTitle("Error", message: "login_email_not_found".localized , type: .Error)
                        
                    })
                    
                    
                    
                default:
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        SwiftMessageBar.showMessageWithTitle("Error", message: "error" , type: .Error)
                        
                    })
                    
                    
                    
                }
                
                
            } else if loginAnswer.result == ResultType.SuccesOld{
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var storyboard:UIStoryboard?
                    storyboard = UIStoryboard(name: "Chillers", bundle: nil)
                    let mainVC = storyboard?.instantiateInitialViewController()
                    let leftMenuVC = storyboard?.instantiateViewControllerWithIdentifier("leftMenu")
                    let slideMenu = SlideMenuController(mainViewController: mainVC!, leftMenuViewController: leftMenuVC!)
                    UIApplication.sharedApplication().keyWindow?.rootViewController = slideMenu
                    UIApplication.sharedApplication().keyWindow?.makeKeyAndVisible()
                    
                })
                
                
                
            } else if loginAnswer.result == ResultType.SuccesNew {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                    let vc = self.storyboard!.instantiateViewControllerWithIdentifier("RegistroViewController")
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                })
                
                
                
                
                //self.performSegueWithIdentifier("NewLogin", sender: self)
                
                
                //self.navigationController?.pushViewController( , animated: true)
                
            }
            
        })
        
    }
    
}
